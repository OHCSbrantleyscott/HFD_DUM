# Utah Healthcare Facility Database (HFD) Data User Manual
February 2022

## Table of Contents

- [Revision History](#revision-history)
- [Introduction](#introduction)
- [Limited Use Data Sets](#limited-use-data-sets)
- [Data Processing and Quality](#data-processing-and-quality)
    - [Data Submission](#data-submission)
    - [System Edits](#system-edits)
    - [Hospital Review](#hospital-review)
    - [Missing Values](#missing-values)
    - [Patient Confidentiality](#patient-confidentiality)
    - [DRG, MS-DRG, APR-DRG, and EAPG Classification](#drg-ms-drg-apr-drg-and-eapg-classification)
    - [Citation](#citation)
- [Available Data and Fill Rates](#available-data-and-fill-rates)
- [Header Variables](#header-variables)
    - [Facility Variables](#facility-variables)
    - [Patient Variables](#patient-variables)
    - [Date & Time Variables](#date-&-time-variables)
    - [Payment & Payer Variables](#payment-&-payer-variables)
    - [Diagnosis Variables](#diagnosis-variables)
    - [Procedure Variables](#procedure-variables)
    - [Provider Variables](#provider-variables)
    - [Grouping Variables](#grouping-variables)
- [Line Variables](#line-variables)




## Revision History

|     Date      |  Description  | Author(s) |
| ------------- | ------------- | --------- |
| November 2019 | Initial Draft | Petersen  |
| February 2020 | Minor edits   | Petersen  |
| March 2020    | Minor edits   | Petersen  |
| February 2022 | Fill rate table changes & edits to better reflect VRW | Scott | 

## Introduction

The Utah Health Data Committee is composed of fifteen governor-appointed members and was
created by the Utah Health Data Authority Act of 1991. The Committee is staffed by the
Office of Health Care Statistics which manages the Utah Healthcare Facility Database.

Utah Administrative Rule requires all Utah licensed hospitals, both general acute care and
specialty, and free standing ambulatory surgical centers to provide data on inpatient,
emergency department, and ambulatory surgery encounters. The Healthcare Facility Database
contains information on patient demographics, admission and discharge, diagnoses, services
received, and charges billed for each encounter.

Data submissions by the FASCs may be incomplete and caution should be used when trying to
perform market level comparisons with these data. Continual efforts are being made to
further data completeness.

Starting in 2016, Huntsman Cancer Institute, Madsen Surgery Center, Moran Eye Center, and
University Orthopedic Center are the only University of Utah Health sites reported
individually; all other University Hospital & Clinics are reported as one facility (#125).

## Limited Use Data Sets

Limited use data sets are available for inpatient, emergency department, and
ambulatory surgery encounters. The limited use data sets are designed to provide general
health care information to a wide spectrum of users with minimal controls.

The limited use data sets include data on procedures, charges, and length of stay. Several factors,
such as case-mix, severity complexity, payer-mix, market areas, hospital ownership,
hospital affiliation, or hospital teaching status, affect the comparability of charge and
length of stay across hospitals. Any analysis of charge or length of stay at the hospital
level should consider the above factors. More information about hospitals can be found in
the [Health Facility Licensing website](http://health.utah.gov/hflcra/facinfo.php).

## Data Processing and Quality

### Data Submission

The Office of Health Care Statistics maintains and publishes the Utah Healthcare Facility
Data Submission Guide on its website. Data suppliers submit all files using specifications
in the data submission guide.

### System Edits

The data are validated through a process of automated editing and report verification.
Each record is subjected to a series of edits that check for validity, consistency,
completeness, and conformity with the definitions specified in the Utah Healthcare
Facility Data Submission Guide. Files that fail edit checks are required to be resubmitted 
by the data supplier for correction.


### Hospital Review

Each hospital is given the opportunity to review and validate findings of the edit checks
and any public report prior to the release of data or information. Inconsistencies
discovered by the facilities are reevaluated or corrected. See [Utah Statute 26-33a-107](https://le.utah.gov/xcode/Title26/Chapter33A/26-33a-S107.html)

### Missing Values

When dealing with unknown values, it is important to distinguish between systematic
omission by the facility (e.g., for facilities that were granted reporting exemption for
particular data elements or which had systematic coding problems that deemed the entire data from the
facility unusable) and non-systematic omission (e.g., random coding errors, invalid codes,
etc.). While systematic omission creates potential bias, non-systematic omission is
assumed to occur randomly. The user is advised to examine missing values by facility for
each data element to be used. The user is likewise advised to examine the number of
observations by facility by quarter to judge if a facility under-reported for a given
quarter, which occasionally happens due to data processing problems experienced by a
facility.

### Patient Confidentiality

The Committee has taken steps to ensure that no individual patient will be identified from
the limited use data sets. Patient’s age, physician specialty, and payers are grouped.
Several data elements are suppressed under specific conditions: 1) ZIP codes with less
than 30 visits in a calendar year are suppressed; 2) physician taxonomy is suppressed for
hospitals with less than 30 beds; 3) Payer names with less than 30 visits in a calendar 
year are suppressed; and 4) age, sex, and ZIP code are suppressed if the discharge involves
substance abuse or HIV infection, as defined by the following Medicare Severity Grouper
Diagnosis Related Groups (MS-DRGs):
 - 894—ALCOHOL, DRUG ABUSE OR DEPENDENCE, LEFT AMA
 - 895—ALCOHOL, DRUG ABUSE OR DEPENDENCE WITH REHABILITATION THERAPY
 - 896—ALCOHOL, DRUG ABUSE OR DEPENDENCE WITHOUT REHABILITATION THERAPY WITH MCC
 - 897—ALCOHOL, DRUG ABUSE OR DEPENDENCE WITHOUT REHABILITATION THERAPY WITHOUT MCC
 - 969—HIV WITH EXTENSIVE O.R. PROCEDURE WITH MCC
 - 970—HIV WITH EXTENSIVE O.R. PROCEDURE WITHOUT MCC
 - 974—HIV WITH MAJOR RELATED CONDITION WITH MCC
 - 975—HIV WITH MAJOR RELATED CONDITION WITH CC
 - 976—HIV WITH MAJOR RELATED CONDITION WITHOUT CC/MCC
 - 977—HIV WITH OR WITHOUT OTHER RELATED CONDITION

### DRG, MS-DRG, APR-DRG, and EAPG Classification

Variables produced by OHCS using 3M grouper software are no longer standard inclusions in
the limited use data sets. Maintenance of the DRG grouper was discontinued in 2007.
Previous versions of limited use data sets may have included variables resulting from the
DRG grouper to aid comparisons to historical data. However, this grouper cannot be applied
to data beginning in 2015 due to the change from ICD-9 to ICD-10 and thus is no is no
longer included in the limited use data sets.

The MS-DRG grouper results are the only grouper results available.

### Citation

Any statistical reporting or analysis based on the data shall cite the source as the
following:

_Utah Healthcare Facility Limited Use Data Sets (2022)_. Utah Health Data Committee/Office
of Health Care Statistics. Utah Department of Health. Salt Lake City, Utah. 2022.


## Available Data and Fill Rates

Fill rates are based on source variables contained in the database. Empty rows are generally
for calculated fields. For a fill rate, see the variable(s) used to calculated the field. 
Please see: https://gitlab.com/UtahOHCS/HFD_DUM/-/blob/master/HFD-fillrates-1996to2020.xlsx for the downloadable tables.


## Header Variables


## Facility Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| encounter_type | Whether the visit was ambulatory/surgery, emergency, or inpatient |
| er_flag | Whether the encounter included emergency services |
| facility_city | Facility city |
| facility_county | Facility county |
| facility_fips_code | Facility FIPS location code |
| facility_geo_class | Whether the facility is considered rural or urban  |
| facility_name | Name of the facility from which the patient was discharged |
| facility_number | OHCS assigned identifier for the facility of discharge |
| facility_state | Facility state |
| facility_street_address | Facility street address |
| facility_zip_code | Facility zip code |
| patient_discharge_status | Codes changed in Oct 2007. See gitlab site for more details.  |
| point_of_origin_for_admission | Code which indicates where patient was admitted |
| type_of_admission | Please see the table on HCUP's admission type website |
| type_of_bill | Maintained by the National Uniform Billing Committee (NUBC). See [Lookup Table B-1.G Type of Bill](https://gitlab.com/UtahOHCS/APCD_DSG/-/blob/master/APCD_UT_DSG.md#b-1g-type-of-bill)
  


## Patient Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| age_group | Text description of age of patient (i.e. 70-74) |
| age_group_num | Numeric code representing the age of patients at release  |
| newborn | Whether the patient is a newborn |
| patient_city | Patient city of residence |
| patient_country | Patient country of residence code |
| patient_county_fips | FIPS code for county of residence. Derived from zip code |
| patient_date_of_birth | Patient date of birth |
| patient_dob_year | Patient year of birth |
| patient_ethnicity | Patient ethnicity. See [Lookup Table B-1.E Ethnicity Codes](https://gitlab.com/UtahOHCS/APCD_DSG/-/blob/master/APCD_UT_DSG.md#b-1e-ethnicity-codes)
| patient_gender | Patient gender (M = male, F = female, U = unknown, S = surpressed)  |
| patient_race | Patient race. See [Lookup Table B-1.D Race Codes](https://gitlab.com/UtahOHCS/APCD_DSG/-/blob/master/APCD_UT_DSG.md#b-1d-race-codes)
| patient_race_ethnicity | Computed using race and ethnicity |
| patient_state | Patient state of residence |
| patient_zip_code | Entire patient zip code as submitted |
| patient_zip5_code | 5 left most digits of the zip code |
| pt_age | Patient's age on day of admission |


## Date & Time Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| admission_hour | The hour at which the patient arrived (using 24 hour format)  |
| admission_minute | The minute the patient arrived at the facility |
| date_of_admission | Admission date |
| discharge_date | Date of discharge |
| discharge_hour | The hour at which the patient left the facility (using 24 hour format)  |
| discharge_minute | The minute at which the patient departed the facility |
| discharge_year | Year of discharge |
| los | Length of stay as calculated from Mercer (Rounds all IP to 1) |
| los_days | Total days stayed at facility from date of admission to discharge |
| los_hours | Total hours stayed at facility |
| quarter | Quarter of discharge |
| statement_beginning_date | The date facility services began |
| statement_beginning_year | The year facility services began |
| statement_through_date | The date facility services ended  |
| statement_through_year | The year facility services ended  |
| year | Year of discharge |
| year_of admission | Admission year |


## Payment & Payer Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| estimated_amount_due_primary | Amount estimated by the hospital to be due from the indicated payer |
| estimated_amount_due_secondary | Amount estimated by the hospital due from the secondary payer |
| estimated_amount_due_tertiary | Amount estimated by the hospital due from the tertiary payer |
| payer_primary_typology_imputed | Imputed payer typology using string-matching on Payer_Name. |
| payer_secondary_typology_imputed | Imputed payer typology using string-matching on Payer_Name. |
| payer_tertiary_typology_imputed | Imputed payer typology using string-matching on Payer_Name. |
| primary_payer_name_suppressed | The payer's name. Suppressed at 30 per year |
| primary_payer_typology | Code with type of payer. See table on gitlab |
| prior_payment_primary | Amount hospital received from payer prior to the billing date |
| prior_payment_secondary | Amount hospital received from secondary payer prior to billing date |
| prior_payment_tertiary | Amount hospital received from tertiary payer prior to billing date |
| secondary_payer_name_suppressed | Secondary payer's name. Suppressed at 30 per year |
| secondary_payer_typology | Code with type of payer. See table on gitlab |
| tertiary_payer_name_suppressed | Tertiary payer's name. Suppressed at 30 per year |
| tertiary_payer_typology | Code with type of payer. See table on gitlab |
| total_charge_header | The total amount charged by the facility for the encounter |


## Diagnosis Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| diagnosis_version_qualifier | Indicates which ICD version was used  |
| principal_diagnosis_code | Codes changed in Oct 2015. ICD diagnosis codes |
| principal_diagnosis_code_poa | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_1 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_1 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_2 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_2 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_3 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_3 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_4 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_4 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_5 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_5 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_6 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_6 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_7 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_7 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_8 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_8 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_9 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_9 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_10 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_10 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_11 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_11 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_12 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_12 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_13 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_13 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_14 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_14 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_15 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_15 | Whether the diagnosis codes were present on admission  |
| secondary_diagnosis_code_16 | Codes changed in Oct 2015. ICD diagnosis codes |
| secondary_diagnosis_code_poa_16 | Whether the diagnosis codes were present on admission  |
| admitting_diagnosis_code | Admitting diagnosis code |
| patient_reason_for_visit_1 | Coded using ICD diagnosis codes |
| patient_reason_for_visit_2 | Coded using ICD diagnosis codes |
| patient_reason_for_visit_3 | Coded using ICD diagnosis codes |
| ext_cause_of_inj_code_1 | External cause code, coded using ICD diagnosis codes |
| ext_cause_of_inj_code_poa_1 | Whether the external cause was present on admission |
| ext_cause_of_inj_code_2 | External cause code, coded using ICD diagnosis codes |
| ext_cause_of_inj_code_poa_2 | Whether the external cause was present on admission |
| ext_cause_of_inj_code_3 | External cause code, coded using ICD diagnosis codes |
| ext_cause_of_inj_code_poa_3 | Whether the external cause was present on admission |


## Procedure Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| principal_ICD_procedure | ICD procedure code. Changed Oct 2015 |
| date_of_principle_procedure | Date of procedure |
| year_of_principle_procedure | Year of procedure |
| secondary_ICD_procedure_1 | ICD procedure code. Changed Oct 2015 |
| date_of_secondary_procedure_1 | Date of procedure |
| year_of_secondary_procedure_1 | Year of procedure |
| secondary_ICD_procedure_2 | ICD procedure code. Changed Oct 2015 |
| date_of_secondary_procedure_2 | Date of procedure |
| year_of_secondary_procedure_2 | Year of procedure |
| secondary_ICD_procedure_3 | ICD procedure code. Changed Oct 2015 |
| date_of_secondary_procedure_3 | Date of procedure |
| year_of_secondary_procedure_3 | Year of procedure |
| secondary_ICD_procedure_4 | ICD procedure code. Changed Oct 2015 |
| date_of_secondary_procedure_4 | Date of procedure |
| year_of_secondary_procedure_4 | Year of procedure |
| secondary_ICD_procedure_5 | ICD procedure code. Changed Oct 2015 |
| date_of_secondary_procedure_5 | Date of procedure |
| year_of_secondary_procedure_5 | Year of procedure |


## Provider Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| attending_provider_qual | Type of provider id (table on gitlab)  |
| attending_provider_id | Unique identifier of attending provider |
| provider_attending_other_type_qualifier | Indicates the type of "other" provider (table on gitlab) |
| provider_attending_taxonomy_code | Provider taxonomy code (using NUCC's code set) |
| provider_attending_AMA_specialty | Provider specialty, coded using AMA's specialty coding system. |
| operating_provider_qual | Type of provider id (table on gitlab)  |
| operating_provider_id | Unique identifier of attending provider |
| provider_operating_other_type_qualifier | Indicates the type of "other" provider (table on gitlab) |
| provider_operating_taxonomy_code | Provider taxonomy code (using NUCC's code set) |
| provider_operating_AMA_specialty | Provider specialty, coded using AMA's specialty coding system. |
| other_provider_qual_1 | Type of provider id (table on gitlab)  |
| other_provider_qual_sec_1 | Second id qualifer of a provider other than the main provider |
| other_provider_id_1 | Unique identifier of attending provider |
| provider_other_1_other_type_qualifier | Indicates the type of "other" provider (table on gitlab) |
| provider_other_1_taxonomy_code | Provider taxonomy code (using NUCC's code set) |
| provider_other_1_AMA_specialty | Provider specialty, coded using AMA's specialty coding system. |
| other_provider_qual_2 | Type of provider id (table on gitlab)  |
| other_provider_qual_sec_2 | Second id qualifer of the second provider other than the main provider |
| other_provider_id_2 | Unique identifier of attending provider |
| provider_other_2_other_type_qualifier | Indicates the type of "other" provider (table on gitlab) |
| provider_other_2_taxonomy_code | Provider taxonomy code (using NUCC's code set) |
| provider_other_2_AMA_specialty | Provider specialty, coded using AMA's specialty coding system. |
| taxonomy_code | Created based on Attending NPI and NPPES lookup  |
| category | High level provider grouping based on attending provider NPI |


## Grouping Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| CMS_4digit_DRG | This is different from CMS DRG due to the 4th digit, (Severity of Illness)  |
| CMS_4digit_DRG_description | 4 digit DRG written description |
| CMS_cost_weight | See MS-DRG grouper documentation for details |
| CMS_DRG | Medicare Severity-Diagnosis Related Group code |
| CMS_DRG_description | Medicare Severity-Diagnosis Related Group written description |
| CMS_final_ms_indicator | Final medical/surgical indicator |
| CMS_grouperversion_used | Which version of the CMS Grouping Software was applied to the service |
| CMS_mdc | Major disease category codes as defined by the MS-DRG |
| CMS_mdc_description | Major disease category descriptions as defined by the MS-DRG |
| diagnosis_related_group | Diagnosis related group as reported by the hospital |
| dxccsr_category_op | Groups Principal Diagnosis Code values into clinical categories  |
| final_DRG_cc_mcc_usage | See MS-DRG grouper documentation for details |
| hospital_acquired_condition_status | See MS-DRG grouper documentation for details |
| hospital_acquired_conditions | See MS-DRG grouper documentation for details |
| msgmce_version_used | MSGMCE software version used for assigning the MS-DRG |




## Line Variables
|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| record_id | A unique number for each visit |
| encounter_type | Whether the visit was ambulatory/surgery, emergency, or inpatient |
| service_line | Count of services. See gitlab for more information |
| HCPCS_Category | High-level CPT-4 grouping.  |
| HCPCS_CCS_Category_Code | Groups HCPCS Code values into one of 240+ clinical categories |
| HCPCS_CCS_Category_Description | Written description for HCPCS CSS code |
| hcpcs_code | Healthcare Common Procedural Coding System  |
| hcpcs_code_mod1 | Produced when modifier clarifies/improves the a procedure code |
| hcpcs_code_mod2 | Produced when modifier clarifies/improves the a procedure code |
| hcpcs_code_mod3 | Produced when modifier clarifies/improves the a procedure code |
| hcpcs_code_mod4 | Produced when modifier clarifies/improves the a procedure code |
| HCPCS_Code1_Desc | Written description of the HCPCS code |
| HCUP_Surgery_Flag | Classifies HCPCS surgery codes as broad, narrow, or neither |
| NDC | Available starting CY2018. Drug code |
| revenue_code | Codes that identify specific accommodations or arrangements |
| Revenue_Code1_Desc | Written description of the revenue code |
| Service_Date | Service Line Date. Available starting with data submitted CY2018.  |
| service_units | The quantity of units, times, days, visits, services, or treatments  |
| Service_Year | Service Line Year. Available starting with data submitted CY2018.  |
| total_charge_by_revenue_code | The total amount charged by the hospital for the given service line |
| unit_of_measurement | Indicates the type of Service Units, e.g., days |
| Colonoscopy_Flag | Indicates if the HCPS code is a colonoscopy |
